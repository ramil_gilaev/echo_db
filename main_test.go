package main

import (
	"log"
	"testing"

	"t1/db"
	"t1/models"
)

type TestDBClient struct{}

var TitlesList = []string{
	"title1", "title2", "title3", "title4", "title5", "title6", "title7", "title8", "title9", "title10",
	"title11", "title12", "title13", "title14", "title15", "title16", "title17", "title18", "title19", "title20", "test",
}

func TestDBSetTitle(t *testing.T) {
	dbConn := db.GetDBConnect()

	if dbConn.Error != nil {
		t.Error(dbConn.Error)
	}

	for _, title := range TitlesList {
		_, err := SetTitle(dbConn.DB, title)

		if err != nil {
			t.Error(err)
		}
	}
}

func TestDBGetTitle(t *testing.T) {
	var err error

	titles := []models.Title{}
	dbConn := db.GetDBConnect()

	if dbConn.Error != nil {
		t.Error(dbConn.Error)
	}

	titles, err = GetTitles(dbConn.DB, "", 3, 3)

	if err != nil {
		t.Error(err)
	}

	log.Println(titles)

	titles, err = GetTitles(dbConn.DB, "test", 1, 10)

	if err != nil {
		t.Error(err)
	}

	log.Println(titles)
}

func TestGetLimitSQL(t *testing.T) {
	testValues := []struct {
		page       int
		perPage    int
		wantLimit  string
		wantResult bool
	}{
		{1, 10, " LIMIT 0, 10", true},
		{1, 10, "LIMIT 0, 10", false},
		{1, 9, " LIMIT 0, 10", false},
		{2, 5, " LIMIT 5, 5", true},
		{0, 10, " LIMIT 0, 10", true},
		{3, 7, " LIMIT 2, 10", false},
		{8, 7, " LIMIT 49, 7", true},
	}

	for key, values := range testValues {
		limit := GetLimitSQL(values.page, values.perPage)
		isTrue := limit == values.wantLimit

		if isTrue != values.wantResult {
			t.Errorf("GetLimitSQL is bad: %d ('%s' vs '%s')", key, limit, values.wantLimit)
		}
	}
}
