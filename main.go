package main

import (
	"fmt"

	"t1/models"

	"gorm.io/gorm"
)

type BaseDBClient interface {
	Create(interface{}) *gorm.DB
	Raw(string, ...interface{}) *gorm.DB
}

func SetTitle(db BaseDBClient, titleString string) (int64, error) {
	title := models.Title{Title: titleString}
	result := db.Create(&title)

	if result.Error != nil {

		return 0, result.Error
	}

	return title.ID, nil
}

func GetTitles(db BaseDBClient, q string, page, perPage int) (titles []models.Title, err error) {
	var where, limit string

	limit = GetLimitSQL(page, perPage)

	if len(q) != 0 {
		where = fmt.Sprintf(" WHERE title LIKE '%%%s%%'", q)
	}

	result := db.Raw(fmt.Sprintf("SELECT * FROM titles %s %s", where, limit)).Find(&titles)

	if result.Error != nil {

		return nil, result.Error
	}

	return titles, nil
}

func GetLimitSQL(page, perPage int) string {

	if page <= 0 {
		page = 1
	}

	if perPage <= 0 {
		perPage = 10
	}

	return fmt.Sprintf(" LIMIT %d, %d", ((page - 1) * perPage), perPage)
}

func main() {

}
