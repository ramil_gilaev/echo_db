package db

import (
	"errors"
	"fmt"
	"log"
	"os"
	"sync"

	"t1/models"

	"github.com/joho/godotenv"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type DBConnect struct {
	DB    *gorm.DB
	Error error
}

var connect *DBConnect
var once sync.Once

func init() {
	dbConn := GetDBConnect()

	if dbConn.Error == nil {
		dbConn.DB.AutoMigrate(&models.Title{})
	} else {
		log.Fatal(dbConn.Error)
	}
}

func GetDBConnect() *DBConnect {
	once.Do(func() {
		err := godotenv.Load("config.env")
		if err == nil {
			var (
				user     = os.Getenv("MYSQL_USER")
				password = os.Getenv("MYSQL_PASSWORD")
				dbName   = os.Getenv("MYSQL_DATABASE")
				host     = os.Getenv("MYSQL_HOST")
			)
			connect = &DBConnect{}

			dbURI := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", user, password, host, dbName)

			connect.DB, connect.Error = gorm.Open(mysql.New(mysql.Config{
				DSN: dbURI,
			}), &gorm.Config{})
		} else {
			connect.Error = errors.New("Env file error")
		}

	})

	return connect
}
